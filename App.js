import React, { useEffect, useReducer, useMemo, createContext } from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import LoginScreen from './src/pages/login';
import RegisterScreen from './src/pages/register';
import MainScreen from './src/pages/main';
import HomeScreen from './src/pages/home';
import PdfScreen from './src/pages/pdf';
import HoleriteScreen from './src/pages/holerite';
import {  ActivityIndicator, View, Dimensions } from 'react-native';
import {  logout, getToken, login, getUser } from './src/services/auth';
import AuthContext from './src/contexts/auth';
import api from './src/services/api';
import OneSignal from 'react-native-onesignal';
import {OS_KEY} from "@env";

const Stack = createStackNavigator();



export default function App() {

  const [state, dispatch] = useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            userToken: action.token,
            isLoading: false,
          };
        case 'SIGN_IN':
          return {
            ...prevState,
            isSignout: false,
            userToken: action.token,
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            isSignout: true,
            userToken: null,
          };
      }
    },
    {
      isLoading: true,
      isSignout: false,
      userToken: null,
    }
  );

  useEffect(() => {    

    async function oneSignalMount  () {
      /* O N E S I G N A L   S E T U P */
      OneSignal.setAppId(OS_KEY);
      OneSignal.setLogLevel(6, 0);
      OneSignal.setRequiresUserPrivacyConsent(false);
      OneSignal.promptForPushNotificationsWithUserResponse(response => {
          console.log("Prompt response:", response);
      });
    
      /* O N E S I G N A L  H A N D L E R S */
      // OneSignal.setNotificationWillShowInForegroundHandler(notifReceivedEvent => {
      //     console.log("OneSignal: notification will show in foreground:", notifReceivedEvent);
      //     let notif = notifReceivedEvent.getNotification();
    
      //     const button1 = {
      //         text: "Cancel",
      //         onPress: () => { notifReceivedEvent.complete(); },
      //         style: "cancel"
      //     };
    
      //     const button2 = { text: "Complete", onPress: () => { notifReceivedEvent.complete(notif); }};
    
      //     Alert.alert("Complete notification?", "Test", [ button1, button2], { cancelable: true });
      // });
      OneSignal.setNotificationOpenedHandler(notification => {
          console.log("OneSignal: notification opened:", notification);
      });
      OneSignal.setInAppMessageClickHandler(event => {
          console.log("OneSignal IAM clicked:", event);
      });
      OneSignal.addEmailSubscriptionObserver((event) => {
          console.log("OneSignal: email subscription changed: ", event);
      });
      OneSignal.addSubscriptionObserver(event => {
          console.log("OneSignal: subscription changed:", event);
      });
      OneSignal.addPermissionObserver(event => {
          console.log("OneSignal: permission changed:", event);
      });
    
      const deviceState = await OneSignal.getDeviceState();

      const token = await getToken();

      if (token) {
        const user = await getUser();
        try{
           const postos = await api.post('/save-onsesignal-id.php', {
             user_id: user.id,
            os_id: deviceState.userId
          });
          console.log('-------->', postos);
        }catch(e){}
      }
      console.log('devicestate: ', deviceState);      
    }

    oneSignalMount();

    // Fetch the token from storage then navigate to our appropriate place
    const bootstrapAsync = async () => {
      let userToken;

      try {
        userToken = await getToken();

        const resp = await api.get('/check-user-holerite.php');


        if (!resp.data.success)
        {
          await logout();
          dispatch({ type: 'SIGN_OUT' });

          userToken = null;
        }


      } catch (e) {
        // Restoring token failed
      }

      dispatch({ type: 'RESTORE_TOKEN', token: userToken });
    };

    bootstrapAsync();

  }, []);

  const authContext = useMemo(
    () => ({
      signIn: async data => {
        await login(data.token, data.usuario);
        dispatch({ type: 'SIGN_IN', token: data.token });
      },
      signOut: async () => {
        await logout();
        dispatch({ type: 'SIGN_OUT' })
      },
    }),
    []
  );

  const deviceHeight = Dimensions.get('window').height;

  function SplashScreen() {
    return (
      <View style={{
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#DB4C21'
      }}>

        <View style={{
          position: 'absolute',
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          top: deviceHeight / 2.2
        }}>

          <ActivityIndicator size="large" color="#FFF" />

        </View>

      </View>
    );
  }


  return (
    <AuthContext.Provider value={authContext}>
      {state.isLoading ? (
        <NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen options={{ headerShown: false }} name="Splash" component={SplashScreen} />
          </Stack.Navigator>
        </NavigationContainer>
      ) :
        state.userToken == null ? (
          <NavigationContainer>
            <Stack.Navigator initialRouteName="Login">
              <Stack.Screen options={{ headerShown: false }} name="Login" component={LoginScreen} />
              <Stack.Screen options={{ headerShown: false }} name="Register" component={RegisterScreen} />
            </Stack.Navigator>
          </NavigationContainer>
        ) : (
          <NavigationContainer>
            <Stack.Navigator initialRouteName="Home">
              <Stack.Screen options={{ headerShown: false }} name="Home" component={HomeScreen} />
              <Stack.Screen options={{ title: 'Meu Holerite' }} name="Main" component={MainScreen} />
              <Stack.Screen options={{}} name="Meu Holerite" component={HoleriteScreen} />
              <Stack.Screen options={{}} name="Holerite" component={PdfScreen} />
            </Stack.Navigator>
          </NavigationContainer>
        )}
    </AuthContext.Provider>
  );
}