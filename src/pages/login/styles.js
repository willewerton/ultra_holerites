import styled from 'styled-components';

const Container = styled.View`
  flex: 1;
  alignItems: center;
  justifyContent: center;
  backgroundColor: #FFF;
`;

const Logo = styled.Image`
  height: 30%;
  marginTop: 80px;
`;

const Form = styled.ScrollView`
  paddingTop: 60px;  
  width: 100%;
  height: 70%;
  borderTopLeftRadius: 60px;
  borderTopRightRadius: 60px;
  backgroundColor: #444444;
`;

const Input = styled.TextInput`
  paddingHorizontal: 20px;
  paddingVertical: 15px;
  borderRadius: 5px;
  backgroundColor: #F5F5F5;
  alignSelf: stretch;
  marginBottom: 15px;
  marginHorizontal: 20px;
  fontSize: 16px;
`;

const ErrorMessage = styled.Text`
  textAlign: center;
  color: #ce2029;
  fontSize: 16px;
  marginBottom: 15px;
  marginHorizontal: 20px;
`;

const Button = styled.TouchableHighlight`
  padding: 20px;
  borderRadius: 5px;
  backgroundColor: #DB4C21;
  alignSelf: stretch;
  margin: 15px;
  marginHorizontal: 20px;
`;

const ButtonText = styled.Text`
  color: #FFF;
  fontWeight: bold;
  fontSize: 16px;
  textAlign: center;
`;

const SignUpLink = styled.TouchableHighlight`
  padding: 10px;
  marginTop: 10px;
`;

const SignUpLinkText = styled.Text`
  color: #FFF;
  fontWeight: 300;
  fontSize: 16px;
  textAlign: center;
`;

export { Container, Form, Logo, Input, ErrorMessage, Button, ButtonText, SignUpLink, SignUpLinkText };