import React, { useState, useContext } from 'react';

import { StatusBar, ActivityIndicator, Alert } from 'react-native';
import { isAuthenticated, login } from '../../services/auth';
import AuthContext from '../../contexts/auth';

import validarCpf from 'validar-cpf';

import api from '../../services/api';

import {
  Container,
  Form,
  Logo,
  Input,
  ErrorMessage,
  Button,
  ButtonText,
  SignUpLink,
  SignUpLinkText,
} from './styles';

export default function Login({ navigation }) {

  const [cpf, setCpf] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);

  const { signIn } = useContext(AuthContext);

  const resetState = () => {
    setCpf('');
    setPassword('');
    setError('');
    setLoading(false);
  };

  const handleCpfChange = (cpf) => {
    cpf = cpf.replace(/\D+/g, '');

    setCpf(cpf);
    if (cpf !== '' && !validarCpf(cpf)) {
      setError('CPF Inválido');
    } else {
      setError('');
    }
  };

  const handlePasswordChange = (password) => {
    setPassword(password);
  };

  const handleCreateAccountPress = () => {
    navigation.navigate('Register');
  };
  const handleLoginPress = () => {
    if (password === '' || cpf === '' || error !== '') {
      Alert.alert(
        'Atenção!',
        'Preencha os dados corretamente'
      );
      return;
    }

    setLoading(true);

    api.post('/criar_sessao_holerite.php', {
      cpf: cpf,
      senha: password,
    }).then(response => {
      const data = response.data;

      if (!data.success) {
        Alert.alert(
          'Atenção!',
          data.error,
        );

        return;
      }
      signIn(data);      
    })
      .catch(e => {
        console.log(JSON.stringify(e));
        Alert.alert(
          'Atenção!',
          'Ocorreu um erro ao tentar se comunicar com o servidor da empresa.',
        );

      }).then(() => {
        setLoading(false);
      });
  };

  const ButtonTextRender = (text) => {
    if (loading) {
      return (<ActivityIndicator size="small" color="#FFF" />);
    } else {
      return (<ButtonText>{text}</ButtonText>)
    }
  };


  return (
    <Container>
      <StatusBar hidden />
      <Logo source={require('../../images/logo-grupo-ultra.jpg')} resizeMode="contain" />

      <Form>
        <Input
          placeholder="CPF"
          value={cpf}
          onChangeText={handleCpfChange}
          autoCapitalize="none"
          keyboardType="numeric"
          maxLength={11}
          autoCorrect={false}
        />
        <Input
          placeholder="Senha"
          value={password}
          onChangeText={handlePasswordChange}
          autoCapitalize="none"
          autoCorrect={false}
          secureTextEntry
          maxLength={8}
        />
        {error.length !== 0 && <ErrorMessage>{error}</ErrorMessage>}
        <Button onPress={handleLoginPress}>
          {ButtonTextRender('Entrar')}
        </Button>
        <SignUpLink onPress={handleCreateAccountPress}>
          <SignUpLinkText>Criar Conta</SignUpLinkText>
        </SignUpLink>
      </Form>
    </Container>
  );
}


