import styled from 'styled-components';

const Container = styled.View`
  flex: 1;
  alignItems: center;
  padding: 20px 0;
  backgroundColor: #FFF;
`;

const Content = styled.View`
    width: 80%;
`;

const TextWelcome = styled.Text`
    fontSize: 26px;
    textAlign: center;
    paddingTop: 30px;
`;

const TextPicker = styled.Text`
    fontSize: 14px;
`;

const ContentPicker = styled.View`
    paddingBottom: 10px;
`;

const ContentChart = styled.View`   
    flex: 1;
    justifyContent: center
`;


const ButtonLink = styled.TouchableHighlight`
    marginTop: 100px;
    padding: 20px;
    backgroundColor: #FFF;
    borderWidth: 1px;
    borderColor: #DB4C21;
    width: 100%;
`;

const ButtonLinkText = styled.Text`
    color: #DB4C21;
    fontWeight: bold;
    fontSize: 16px;
    textAlign: center;
`;

const ButtonView = styled.TouchableHighlight`
    padding: 20px;
    backgroundColor: #DB4C21;
    position: absolute;
    left: 0;
    bottom:0;
    width: 100%;
`;

const ButtonViewText = styled.Text`
    color: #FFF;
    fontWeight: bold;
    fontSize: 16px;
    textAlign: center;
`;

const LoadingContent = styled.View`
    position: absolute;
    flex: 1;
    justifyContent: center;
    alignItems: center;
    backgroundColor: #F8F8F8;
    padding: 20px 40px;
    opacity: 0.9;
    borderRadius: 10px;
    shadowColor: #000;
    shadowOpacity: 0.25;
    shadowRadius: 4px;
    elevation: 5;

`;


const TextVersion = styled.Text`
    marginTop: 60px;
    textAlign: center;
`;


export { Container, Content, TextWelcome, TextPicker, ContentPicker, ContentChart, ButtonLink, ButtonLinkText, ButtonView, ButtonViewText, LoadingContent, TextVersion }