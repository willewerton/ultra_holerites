import React, { useContext, useState, useEffect } from 'react';
import { Container, Content, TextWelcome, ButtonLink, ButtonLinkText, ButtonView, ButtonViewText, LoadingContent, TextVersion } from './styles';
import { StatusBar, Alert, ActivityIndicator, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import api from '../../services/api';
import AuthContext from '../../contexts/auth';
import { getUser } from '../../services/auth';

import pk from '../../../package.json';

export default function Home({ navigation }) {
    const { signOut } = useContext(AuthContext);

    const [loading, setLoading] = useState(false);
    const [userName, setUserName] = useState('');

    useEffect(() => {
        async function getUserName () {
            const user = await getUser();
            var userNameArr = user.nome.split(' ');
            setUserName(userNameArr[0]);
        } 

        getUserName();

    }, []);

    const doLogout = () => {

        Alert.alert('Atenção', 'Tem certeza que deseja sair?', [
            {
                text: 'Cancelar',
                onPress: () => console.log('Cancelado'),
                style: 'cancel'
            },
            {
                text: 'Sim',
                onPress: async () => {

                    try {
                        setLoading(true);
                        await api.get('/encerrar_sessao_holerite.php')
                        signOut();
                    } catch (error) {
                        console.log(JSON.stringify(error));
                    }

                    setLoading(false);
                }
            }
        ]);
    }

    const deviceHeight = Dimensions.get('window').height;

    return (
        <Container>
            <StatusBar />
            <Content>
                <TextWelcome>
                    Olá, {userName}
                </TextWelcome>

                <ButtonLink onPress={() => navigation.navigate('Main')}>
                    <ButtonLinkText>
                        Meus Holerites
                    </ButtonLinkText>
                </ButtonLink>

                <TextVersion>Versão {pk.version}</TextVersion>
            </Content>


            <ButtonView onPress={() => doLogout()}>
                <ButtonViewText><Icon name="logout" /> Sair</ButtonViewText>
            </ButtonView>

            {loading &&
                <LoadingContent style={{ top: deviceHeight / 3 }}>
                    <ActivityIndicator size="large" color="#000" />
                </LoadingContent>}
        </Container>
    );
}