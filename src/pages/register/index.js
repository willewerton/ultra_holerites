import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { StatusBar, ActivityIndicator, Alert } from 'react-native';

import validarCpf from 'validar-cpf';

import api from '../../services/api';

import {
  Container,
  Form,
  Logo,
  Input,
  ErrorMessage,
  Button,
  ButtonText,
  SignUpLink,
  SignUpLinkText,
  Loading
} from './styles';

export default class Register extends Component {


  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
      dispatch: PropTypes.func,
    }).isRequired,
  };

  state = {
    form: 1,
    cpf: '',
    phone: '',
    nome: '',
    empresa: '',
    num_cad: '',
    password: '',
    password_confirmation: '',
    error: '',
    loading: false
  };

  handleCpfChange = (cpf) => {
    cpf = cpf.replace(/\D+/g, '');

    this.setState({ cpf });
    if (cpf !== '' && !validarCpf(cpf)) {
      this.setState({ error: 'CPF Inválido' });
    } else {
      this.setState({ error: '' });
    }
  };

  handlePhoneChange = (phone) => {
    phone = phone.replace(/\D+/g, '');

    this.setState({ phone });
  };

  handlePasswordChange = (password) => {

    this.setState({ password });
  };

  handlePasswordConfirmationChange = (password_confirmation) => {

    this.setState({ password_confirmation });

    if (this.state.password !== password_confirmation) {
      this.setState({ error: 'Senhas não conferem!' });
    } else {
      this.setState({ error: '' });
    }
  };

  handleBackTologinPress = () => {
    this.props.navigation.navigate('Login');
  };

  resetState = () => {
    this.setState({
      form: 1,
      cpf: '',
      phone: '',
      nome: '',
      empresa: '',
      num_cad: '',
      password: '',
      password_confirmation: '',
      error: '',
      loading: false
    });
  };

  handleSendPress = () => {

    if ((this.state.nome === '' || this.state.cpf === '' || this.state.phone === '') && this.state.error !== '') {
      Alert.alert(
        'Atenção!',
        'Algum dado não foi passado corretamente!'
      );
      return;
    }

    if (this.state.password === '' || this.state.password_confirmation === '') {
      Alert.alert(
        'Atenção!',
        'Preencha as senhas corretamente'
      );
      return;
    }

    this.setState({ loading: true });

    api.post('/salvar_usuario_holerite.php', {
      cpf: this.state.cpf,
      telefone: this.state.phone,
      nome: this.state.nome,
      empresa: this.state.empresa,
      num_cad: this.state.num_cad,
      password: this.state.password,
      password_confirmation: this.state.password_confirmation
    }).then(response => {
      const data = response.data;

      if (!data.success) {
        Alert.alert(
          'Atenção!',
          data.error,
        );

        return;
      }

      if (!data.rows) {
        Alert.alert(
          'Atenção!',
          'Ocorreu um erro ao tentar salvar o usuário, por favor tente novamente'
        );

        return;
      }

      this.resetState();

      Alert.alert(
        'Sucesso!',
        'Agora é só fazer login no aplicativo'
      );

      this.props.navigation.navigate('Login');

    })
      .catch(e => {
        Alert.alert(
          'Atenção!',
          'Ocorreu um erro ao tentar se comunicar com o servidor da empresa.',
        );

      }).then(() => {
        this.setState({ loading: false });
      });
  }


  handleFormOnePress = () => {

    if (this.state.cpf !== '' && this.state.phone !== '' && this.state.error !== '') {
      return;
    }

    this.setState({ loading: true });

    api.get('/busca_usuario.php?cpf=' + this.state.cpf + '&telefone=' + this.state.phone).then(response => {
      const data = response.data;

      console.log(data);

      if (!data.success) {
        Alert.alert(
          'Atenção!',
          data.error,
        );

        return;
      }

      if (!data.rows.length) {
        Alert.alert(
          'Atenção!',
          'Funcionário não encontrado'
        );

        return;
      }

      Alert.alert(
        'Sucesso!',
        'Agora escolha uma senha para acessar o aplicativo'
      );

      this.setState({
        nome: data.rows[0].NomFun,
        num_cad: data.rows[0].NumCad,
        empresa: data.rows[0].NumEmp,
        form: 2
      });
    })
      .catch(e => {
        Alert.alert(
          'Atenção!',
          'Ocorreu um erro ao tentar se comunicar com o servidor da empresa.',
        );

      }).then(() => {
        this.setState({ loading: false });
      });
  };

  ButtonTextRender = (text) => {
    if (this.state.loading) {
      return (<ActivityIndicator size="small" color="#FFF" />);
    } else {
      return (<ButtonText>{text}</ButtonText>)
    }
  };


  backtoLogin = () => {
    return (
      <SignUpLink onPress={this.handleBackTologinPress}>
        <SignUpLinkText>Voltar pro Login</SignUpLinkText>
      </SignUpLink>
    );
  }

  form = () => {

    if (this.state.form === 1) {

      return (
        <Form>
          <Input
            placeholder="CPF"
            value={this.state.cpf}
            onChangeText={this.handleCpfChange}
            autoCapitalize="none"
            keyboardType="numeric"
            maxLength={11}
            autoCorrect={false}
          />

          <Input
            placeholder="Telefone com DDD"
            value={this.state.phone}
            onChangeText={this.handlePhoneChange}
            autoCapitalize="none"
            keyboardType="numeric"
            maxLength={11}
            autoCorrect={false}
          />

          {this.state.error.length !== 0 && <ErrorMessage>{this.state.error}</ErrorMessage>}
          <Button onPress={this.handleFormOnePress}>
            {this.ButtonTextRender('Próximo')}
          </Button>
          {this.backtoLogin()}
        </Form>
      );
    }

    if (this.state.form === 2) {
      return (
        <Form>
          <Input
            placeholder="Senha"
            value={this.state.password}
            onChangeText={this.handlePasswordChange}
            autoCapitalize="none"
            keyboardType="numeric"
            maxLength={11}
            autoCorrect={false}
            secureTextEntry
          />

          <Input
            placeholder="Confirme a Senha"
            value={this.state.password_confirmation}
            onChangeText={this.handlePasswordConfirmationChange}
            autoCapitalize="none"
            keyboardType="numeric"
            maxLength={11}
            autoCorrect={false}
            secureTextEntry
          />

          {this.state.error.length !== 0 && <ErrorMessage>{this.state.error}</ErrorMessage>}
          <Button onPress={this.handleSendPress}>
            {this.ButtonTextRender('Enviar')}
          </Button>

          {this.backtoLogin()}

        </Form>
      );
    }

  }

  render() {

    return (
      <Container>
        <StatusBar hidden />
        <Logo source={require('../../images/logo-grupo-ultra.jpg')} resizeMode="contain" />

        {this.form()}

      </Container>
    );

  }

}


