import styled from 'styled-components';

const Form = styled.ScrollView`
  paddingTop: 60px;  
  width: 100%;
  height: 70%;
  borderTopLeftRadius: 60px;
  borderTopRightRadius: 60px;
  backgroundColor: #444444;
`;

const Container = styled.View`
  flex: 1;
  alignItems: center;
  justifyContent: center;
  backgroundColor: #FFF;
`;

const Logo = styled.Image`
  height: 30%;
  marginTop: 80px;
`;

const Input = styled.TextInput`
  paddingHorizontal: 20px;
  paddingVertical: 15px;
  borderRadius: 5px;
  backgroundColor: #FFF;
  alignSelf: stretch;
  marginBottom: 15px;
  marginHorizontal: 20px;
  fontSize: 16px;
`;

const Alert = styled.View`
  backgroundColor: #f8d7da;
  width: 100%;
  height: 60px;
`;

const ErrorMessage = styled.Text`
  backgroundColor: #f8d7da;
  width: 90%;
  height: 40px;
  textAlign: center;
  alignSelf: stretch;
  paddingTop:9px;
  fontWeight: 300;
  color: #ce2029;  
  fontSize: 16px;
  marginBottom: 15px;
  marginHorizontal: 20px;
`;

const Button = styled.TouchableHighlight`
  padding: 20px;
  borderRadius: 5px;
  backgroundColor: #DB4C21;
  alignSelf: stretch;
  margin: 15px;
  marginHorizontal: 20px;
`;

const ButtonText = styled.Text`
  color: #FFF;
  fontWeight: bold;
  fontSize: 16px;
  textAlign: center;
`;

const SignUpLink = styled.TouchableHighlight`
  padding: 10px;
  marginTop: 5px;
`;

const SignUpLinkText = styled.Text`
    color: #FFF;
    fontWeight: 300;
    fontSize: 16px;
    textAlign: center;
`;

const Loading = styled.View`
  height: 100px;
  background: #FFF;
  flex: 1;
  justifyContent: center;
  flexDirection: row;
  padding: 10px;
`;

export { Container, Form, Logo, Input, Alert, ErrorMessage, Button, ButtonText, SignUpLink, SignUpLinkText, Loading };