import React, { useState, useEffect } from 'react';
import { FAB } from 'react-native-paper';
import { View, Dimensions, PermissionsAndroid, Alert } from 'react-native';
import Pdf from 'react-native-pdf';
import RNFetchBlob from 'rn-fetch-blob';
import {API_URL} from "@env";


export default function PDF({ route, navigation }) {

    const { calculo, empresa, cpf } = route.params;

    const source = { uri: `${API_URL}gerar_pdf_holerite.php?calculo=${calculo}&empresa=${empresa}&cpf=${cpf}`, cache: false };

    const Downloadpdf = async () => {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            {
              title: "Permissão para salvar o arquivo",
              message: "O App precisa de permissão para salvar o arquivo.",
              buttonNeutral: "Pergunte-me depois",
              buttonNegative: "Cancelar",
              buttonPositive: "OK"
            }
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            let dirs = RNFetchBlob.fs.dirs;
            let date = new Date();

            RNFetchBlob
            .config({
                // add this option that makes response data to be stored as a file,
                // this is much more performant.
                fileCache: true,
                addAndroidDownloads : {
                    useDownloadManager : true, // setting it to true will use the device's native download manager and will be shown in the notification bar.
                    notification : false,
                    path:  dirs.DownloadDir  + "/holerite_"+Math.floor(date.getTime() + date.getSeconds() / 2) + '.pdf', // this is the path where your downloaded file will live in
                    description : 'Baixando Holerite.'
                  }
            })
            .fetch('GET', source.uri, {
                //some headers ..
            })
            .then((res) => {
                Alert.alert(
                    'Sucesso!',
                    'O holerite foi salvo na pasta de downloads',
                );
                // the temp file path
                console.log('The file saved to ', res.path())
            })
          } else {
            Alert.alert(
                'Atenção!',
                'O App não tem permissão para salvar arquivos',
            );
          }         
    };

    return (
        <View style={{
            flex: 1,
            justifyContent: 'flex-start',
            alignItems: 'center',
            marginTop: 25,
        }}>
            <Pdf
                source={source}
                onLoadComplete={(numberOfPages, filePath) => {
                    console.log(`number of pages: ${numberOfPages}`, source);
                }}
                onPageChanged={(page, numberOfPages) => {
                    console.log(`current page: ${page}`);
                }}
                onError={(error) => {
                    console.log(error);
                }}
                onPressLink={(uri) => {
                    console.log(`Link presse: ${uri}`)
                }}
                style={{
                    flex: 1,
                    width: Dimensions.get('window').width,
                    height: Dimensions.get('window').height,
                }} />

            <FAB
                style={{
                    position: 'absolute',
                    bottom: 0,
                    right: 0,
                    margin: 20,
                    backgroundColor: "#DB4C21"
                }}
                color="#FFF"
                icon="file-download"
                onPress={() => Downloadpdf()}
            />
        </View>
    );
}