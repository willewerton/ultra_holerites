import React, { useState, useEffect } from 'react';
import { Picker } from '@react-native-picker/picker';
import { Dimensions, Alert, ActivityIndicator, StatusBar } from 'react-native';

import { Container, Content, ContentPicker, TextPicker, LoadingContent, ButtonView, ButtonViewText } from './styles';
import moment from 'moment';
import api from '../../services/api';


export default function Main({ navigation }) {

    const [empresa, setEmpresa] = useState('');
    const [empresas, setEmpresas] = useState([]);
    const [ano, setAno] = useState([]);
    const [anos, setAnos] = useState([]);
    const [calculos, setCalculos] = useState([]);
    const [calculo, setCalculo] = useState([]);
    const [loading, setLoading] = useState(false);


    useEffect(() => {

        async function getEmpresaUser() {
            try {

                const response = await api.get('/get_empresa_user.php');
                if (!response.data.success) {
                    return;
                }
                setEmpresa(response.data.user.empresa + "-principal");
            } catch (e) {
            }
        }

        getEmpresaUser();

        async function getEmpresas() {
            try {

                const response = await api.get('/empresas.php');
                console.log('getEmpresas:get', JSON.stringify(response));
                if (!response.data.success) {                  

                    return;
                }
                setEmpresas(response.data.rows);
            } catch (e) {
                
            }
        }

        getEmpresas();

        async function getAnos() {

            try {

                const response = await api.get('/anos.php');
                // console.log('getEmpresas:get', response);
                if (!response.data.success) {

                    return;
                }
                setAnos(response.data.anos);
            } catch (e) {
                
            }

        }

        getAnos();

        setAno(new Date().getFullYear());
    }, []);

    useEffect(() => {
        getCalculos();
    }, [empresa, ano]);
    
    useEffect(() => {
        if (calculos.length) {
            setCalculo(calculos[0].CodCal);
        }
    }, [calculos]);



    const getCalculos = async () => {

        try {
            const response = await api.get('/buscar_calculos.php?ano=' + ano + '&empresa=' + empresa);

            console.log('getcalculos:get', response);
            if (!response.data.success) {
                return;
            }
            setCalculos(response.data.rows);
        } catch (e) {
        }
    };

    const labelCalculo = (c) => {
        var calculo = '';
        if (c.TipCal == 11) {
            calculo = 'Cálculo Mensal';
        }

        if (c.TipCal == 91) {
            calculo = 'Adiantamento Salarial';
        }

        if (c.TipCal == 32) {
            calculo = '13ª Salário Integral';
        }

        if (c.TipCal == 31) {
            calculo = 'Adiantamento 13ª Salário';
        }

        if (c.TipCal == 15) {
            calculo = 'Complementar Rescisão';
        }

        var data = moment(c.PerRef).format('MM/YYYY');

        return calculo + ' - ' + data;
    };



    const PickEmpresaRender = () => {

        return (
            <Picker
                selectedValue={empresa}
                onValueChange={(itemValue, itemIndex) => {
                    setEmpresa(itemValue);
                }}>
                {empresas.map((v, k) =>
                    <Picker.Item label={v.ApeEmp} key={k} value={v.NumEmp + '-' + v.base} />
                )}
            </Picker>
        );
    }

    const PickAnosRender = () => {

        return (
            <Picker
                selectedValue={ano}
                onValueChange={(itemValue, itemIndex) => {
                    setAno(itemValue);
                }}>
                {anos.map((v, k) =>
                    <Picker.Item label={v} key={k} value={v} />
                )}
            </Picker>
        );
    }

    const PickCalculosRender = () => {

        return (
            <Picker
                selectedValue={calculo}
                onValueChange={(itemValue, itemIndex) =>
                    setCalculo(itemValue)
                }>
                {calculos.map((v, k) =>
                    <Picker.Item label={labelCalculo(v)} key={k} value={v.CodCal} />
                )}
            </Picker>
        );
    }



    const deviceHeight = Dimensions.get('window').height;

    const showModal = async () => {
        try {
            setLoading(true);
            const response = await api.get('/buscar_holerite.php?calculo=' + calculo + "&empresa=" + empresa);
            setLoading(false);

            console.log(response);

            if (!response.data.success) {

                return;
            }

            if (!response.data.eves.length) {
                Alert.alert(
                    'Atenção!',
                    'Sem informações',
                );

                return;
            }

            navigation.navigate('Meu Holerite', {calculo, empresa, eventos: response.data.eves});
        } catch (e) {
            setLoading(false);
            console.log('getHolerite:error', e);            
        }
    };

    return (

        <Container>
            <StatusBar/>
            <Content>
                <ContentPicker>
                    <TextPicker>Empresa</TextPicker>
                    {PickEmpresaRender()}
                </ContentPicker>

                <ContentPicker>
                    <TextPicker>Ano</TextPicker>
                    {PickAnosRender()}
                </ContentPicker>
                <ContentPicker>
                    <TextPicker>Referência</TextPicker>
                    {PickCalculosRender()}
                </ContentPicker>
            </Content>
            <ButtonView onPress={showModal}>
                <ButtonViewText>Exibir</ButtonViewText>
            </ButtonView>
            {loading &&
                <LoadingContent style={{ top: deviceHeight / 3 }}>
                    <ActivityIndicator size="large" color="#000" />
                </LoadingContent>}
        </Container>

    );
}