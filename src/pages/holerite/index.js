import React, { useState, useEffect } from 'react';
import { PieChart } from 'react-native-svg-charts';
import { Dimensions, Alert, ScrollView, View, StatusBar } from 'react-native';

import { Portal, Text, List, DataTable } from 'react-native-paper';
import {  ContentChart, ContentList, ButtonView, ButtonViewText } from './styles';

import api from '../../services/api';
import { isAuthenticated, logout, getUser } from '../../services/auth';
import { formatNumber } from '../../services/util';

export default function MeuHolerite({ route, navigation }) {

    const { calculo, empresa, eventos } = route.params;

    const [selectedSlice, setSelectedSlice] = useState({ label: '', value: '' })
    const [labelWidth, setLabelWidth] = useState(0);
    const [expanded, setExpanded] = useState(false);

    //grafico
    const [proventos, setProventos] = useState(0);
    const [EveProventos, setEveProventos] = useState([]);
    const [descontos, setDescontos] = useState(0);
    const [EveDescontos, setEveDescontos] = useState([]);
    const [liquido, setLiquido] = useState(0);
  

    useEffect(() => {
        calcularValores();
    }, [eventos]);

    useEffect(() => {
        setSelectedSlice({ label: 'Proventos', value: formatNumber(proventos) });
    }, [proventos]);   

    const handlePress = () => setExpanded(!expanded);
   
    const calcularValores = () => {
        var proventosV = 0,
            descontosV = 0,
            liquidoV = 0,
            eveProventos = [],
            eveDescontos = [];

        for (let i = 0; i < eventos.length; i++) {
            if (eventos[i].TipEve == 1 || eventos[i].TipEve == 2 || eventos[i].TipEve == 5) {
                proventosV += eventos[i].ValEve * 1;
                eveProventos.push(eventos[i]);
            } else if (eventos[i].TipEve == 4) {
                eveProventos.push(eventos[i]);
            } else {

                descontosV += eventos[i].ValEve * 1;
                eveDescontos.push(eventos[i]);
            }
        }

        liquidoV = proventosV - descontosV;

        setProventos(proventosV);
        setDescontos(descontosV);
        setLiquido(liquidoV);

        setEveProventos(eveProventos);
        setEveDescontos(eveDescontos);

    }

    // const { labelWidth, selectedSlice } = this.state;
    const { label, value } = selectedSlice;
    const keys = ['Líquido', 'Descontos'];
    const values = [liquido, descontos];
    const colors = ['#3CB371', '#FF0000'];
    const data = keys.map((key, index) => {
        return {
            key,
            value: values[index],
            svg: { fill: colors[index] },
            onPress: () => setSelectedSlice({ label: key, value: formatNumber(values[index]) })
        }
    })
    const deviceWidth = Dimensions.get('window').width;    

    return (

        <View style={{flex: 1,
            justifyContent: 'center', backgroundColor: '#FFF'}}>
                <StatusBar />
                <ScrollView>
                    <ContentChart>

                        <PieChart
                            style={{ height: 250 }}
                            outerRadius={'80%'}
                            innerRadius={'60%'}
                            data={data}
                        />

                        <Text
                            onLayout={({ nativeEvent: { layout: { width } } }) => {
                                setLabelWidth(width);
                            }}
                            style={{
                                color:  '#000',
                                position: 'absolute',
                                left: deviceWidth / 2.5,
                                textAlign: 'center'
                            }}>
                            {`${label} \n ${value}`}
                        </Text>

                    </ContentChart>

                    <ContentList>
                        <List.Accordion
                            title="Proventos"
                            titleStyle={{color: '#000'}} 
                            descriptionStyle={{color: '#5c5c5c'}}
                            description={'R$ ' +formatNumber(proventos, '')}>
                            <DataTable>
                                <DataTable.Header>
                                    <DataTable.Title>Descrição</DataTable.Title>
                                    <DataTable.Title numeric>Referência</DataTable.Title>
                                    <DataTable.Title numeric>Vencimentos</DataTable.Title>
                                </DataTable.Header>

                                {EveProventos.map((ep, k) =>

                                    <DataTable.Row key={k}>
                                        <DataTable.Cell >{ep.DesEve}</DataTable.Cell>
                                        <DataTable.Cell numeric>{formatNumber(ep.RefEve, '')}</DataTable.Cell>
                                        <DataTable.Cell numeric>{formatNumber(ep.ValEve)}</DataTable.Cell>
                                    </DataTable.Row>
                                )}

                            </DataTable>

                        </List.Accordion>

                        <List.Accordion
                            title="Descontos"
                            titleStyle={{color: '#000'}}
                            description={'R$ ' +formatNumber(descontos, '')}
                            descriptionStyle={{color: '#FF0000'}}
                            expanded={expanded}
                            onPress={handlePress}>
                            <DataTable>
                                <DataTable.Header>
                                    <DataTable.Title>Descrição</DataTable.Title>
                                    <DataTable.Title numeric>Referência</DataTable.Title>
                                    <DataTable.Title numeric>Descontos</DataTable.Title>
                                </DataTable.Header>

                                {EveDescontos.map((dc, k) =>

                                    <DataTable.Row key={k}>
                                        <DataTable.Cell >{dc.DesEve}</DataTable.Cell>
                                        <DataTable.Cell numeric>{formatNumber(dc.RefEve, '')}</DataTable.Cell>
                                        <DataTable.Cell numeric>{formatNumber(dc.ValEve)}</DataTable.Cell>
                                    </DataTable.Row>
                                )}

                            </DataTable>
                        </List.Accordion>

                        <List.Item
                            title="Líquido"
                            titleStyle={{color: '#000'}}
                            description={'R$ ' +formatNumber(liquido, '')}
                            descriptionStyle={{color: '#3CB371'}}>
                        </List.Item>
                    </ContentList>

                </ScrollView >
                <ButtonView onPress={async () => {
                    const user = await getUser();
                    navigation.navigate('Holerite', { calculo, empresa, cpf: user.cpf })
                }
                }>
                    <ButtonViewText>Visualizar PDF</ButtonViewText>
                </ButtonView>
        </View>

    );
}