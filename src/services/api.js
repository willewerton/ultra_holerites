import axios from 'axios';
import { getToken } from './auth';
import { Alert  } from 'react-native';
import {API_URL} from "@env";

/* Endereços para cada emulador/simulador:
** Genymotion:              http://10.0.3.2:3333/
** Emulador Android Studio: http://10.0.2.2:3333/
** Simulador IOS:           http://localhost:3333/
*/
const api = axios.create({
  baseURL: API_URL,
});


console.log('------------------->', API_URL);

api.interceptors.request.use(async config => {
  const token = await getToken();
  if (token) {
    config.headers.Authorization = `${token}`;
  }
  return config;
});

api.interceptors.response.use(function (response) {

  return response;
}, function (err) {
  if (err.message.indexOf('403') !== -1) {

    Alert.alert(
      'Atenção!',
      'Você foi desconectado, feche o app e abra de novo.',
    );
  } else {
    Alert.alert(
      'Atenção!',
      'Ocorreu um erro ao tentar se comunicar com o servidor da empresa.',
    );
  }
});

export default api;