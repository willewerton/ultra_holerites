import AsyncStorage from '@react-native-async-storage/async-storage';

export const TOKEN_KEY = "@Holerites:token";
export const isAuthenticated = async () => { 
   const token = await AsyncStorage.getItem(TOKEN_KEY);

   return token === null ? false : true;
}

export const getToken = async () => await AsyncStorage.getItem(TOKEN_KEY);
export const getUser = async () => {
  const user = await AsyncStorage.getItem('user');
  return JSON.parse(user);
};

export const login = async (token, user) => {
  await AsyncStorage.setItem(TOKEN_KEY, token);
  await AsyncStorage.setItem('user', JSON.stringify(user));
};

export const logout = async () => {
  await AsyncStorage.removeItem(TOKEN_KEY);
  await AsyncStorage.removeItem('user');
};